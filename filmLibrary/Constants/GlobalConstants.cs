﻿namespace Shop.Constants
{
    public static class GlobalConstants
    {
        public const string PATH_TO_DATA = "~/Data";
        public const string PATH_TO_FILMS_FILE = PATH_TO_DATA + "/films.txt";

    }
}