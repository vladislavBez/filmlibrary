﻿using filmLibrary.Models;
using Newtonsoft.Json;
using Shop.Constants;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Hosting;

namespace filmLibrary.Services
{
    public static class FileService
    {
        public static List<Movie> GetMoviesByFile()
        {
            List<Movie> moviesList = new List<Movie>();
            string path = HostingEnvironment.MapPath(GlobalConstants.PATH_TO_FILMS_FILE);
            string lineStr;

            //Get array of json films from text file
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                while ((lineStr = sr.ReadLine()) != null)
                {
                    moviesList.Add(new Movie
                    {
                        Name = lineStr.Substring(0, lineStr.IndexOf("-")).Trim(),
                        Year = lineStr.Substring(lineStr.IndexOf("-") + 1).Trim()
                    });
                }
            }

            return moviesList;
        }
    }
}