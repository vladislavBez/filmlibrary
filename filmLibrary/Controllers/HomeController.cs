﻿using filmLibrary.Services;
using System.Web.Http;

namespace filmLibrary.Controllers
{
    [AllowCrossSiteJson]
    public class HomeController : ApiController
    {
        //GET: /getFilms'
        [Route("movies")]
        public IHttpActionResult GetFilms()
        {
            return Json(new { movies = FileService.GetMoviesByFile() });
        }
    }
}
